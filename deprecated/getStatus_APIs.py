import unittest
import requests
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Get_Status_Tests(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting getStatus tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_get_wifi_level(self):
        print("Get the status of wifi level of a camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['wifi_level'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('wifi_level', resultado['attributes'][0]['attribute'], "Response was not 'wifi_level'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    def test_get_fw_version(self):
        print("Get the status of firmware version of a camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['fw_version'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('fw_version', resultado['attributes'][0]['attribute'], "Response was not 'fw_version'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")

    @unittest.skip("Needs to be implemented in camera")
    # add check to the value got by the API
    def test_get_wifi_bw(self):
        print("Get the status of wifi bandwidth of a camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['wifi_bw'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('wifi_bw', resultado['attributes'][0]['attribute'], "Response was not 'wifi_bw'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")

    @unittest.skip("Needs to be implemented in camera")
    # add check to the value got by the API
    def test_get_current_position(self):
        print("Get the status and current position of a camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['current_position'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('current_position', resultado['attributes'][0]['attribute'],
                         "Response was not 'current_position'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")

    @unittest.skip("Ready to test")
    def test_get_non_existent_attribute(self):
        print("Get the status of a non existence attribute...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['horiz_flip'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(409, req.status_code, "status_code was not 409")

    @unittest.skip("Needs to be implemented in camera")
    def test_get_usd_presence(self):
        print("Get the status of uSD-card presence of a camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                            json={'attributes': ['usd_presence'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if resultado['attributes'][0]['value'] is True:
            flag_response = True
        elif resultado['attributes'][0]['value'] is False:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('usd_presence', resultado['attributes'][0]['attribute'], "Response was not 'usd_presence'")
        self.assertTrue(flag_response, "Value in response is not correct")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()
