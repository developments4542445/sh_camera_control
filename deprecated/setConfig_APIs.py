import unittest
import requests
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Set_Config_Tests(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting setConfig tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_horiz_flip(self):
        print("Assign values (bool, int, string and null) to the horizontal flip attribute...")
        # Testing case: send a bool
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['horiz_flip', True], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['horiz_flip', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['horiz_flip', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['horiz_flip', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_vert_flip(self):
        print("Assign values (bool, int, string and null) to the vertical flip attribute...")
        # Testing case: send a bool
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['vert_flip', True], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['vert_flip', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['vert_flip', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['vert_flip', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_img_res(self):
        print("Assign values (null, int, string and bool) to the image resolution attribute...")
        # Testing case: send a string
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['img_res', 'sd'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_img_res(self):
        print("Assign values (null, int, string and bool) to the image resolution attribute...")
        # Testing case: send a string
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['img_res', 'sd'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['img_res', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    # Check if POST is correct
    # Define asserts
    def test_set_motion_sensitivity(self):
        print("Assign values (null, int, string and bool) to the motion sensitivity attribute...")
        # Testing case: send a string
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['motion_sensitivity', 'low'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['motion_sensitivity', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['motion_sensitivity', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['motion_sensitivity', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    # Check if POST is correct
    # Define asserts
    def test_set_videoclip_block_zone(self):
        print("Assign values (JSON, null, int, string and bool) to the video clip block zone attribute...")
        # Testing case: send a JSON
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['videoclip_block_zone', '(100,100)'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['videoclip_block_zone', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['videoclip_block_zone', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['videoclip_block_zone', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req5 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['videoclip_block_zone', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req5.status_code))
        print("Content: " + str(req5.content))

        # Checking by content
        resultado5 = req5.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req5.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado5['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado5['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_sound_threshold(self):
        print("Assign values (null, int, string and bool) to the sound threshold attribute...")
        # Testing case: send a string
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['sound_threshold', 'low'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['sound_threshold', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['sound_threshold', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['sound_threshold', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_day_night_threshold(self):
        print("Assign values (null, int, string and bool) to the day / night threshold attribute...")
        # Testing case: send a string
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['day_night_threshold', 'low'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_threshold', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_threshold', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_threshold', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_day_night_automatic(self):
        print("Assign values (bool, int, string and null) to the vertical flip attribute...")
        # Testing case: send a bool
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['day_night_automatic', True], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_automatic', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_automatic', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['day_night_automatic', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_point_of_interest(self):
        print("Assign values (int, null, string and bool) to the point of interest attribute...")
        # Testing case: send an int
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['point_of_interest', 3], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['point_of_interest', None], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['point_of_interest', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['point_of_interest', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    # Check if POST is correct
    # Define asserts
    def test_set_home_position(self):
        print("Assign values (null, int, string and bool) to the point of interest attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['home_position', None], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an int
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['home_position', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['home_position', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['home_position', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    # Check if POST is correct
    # Define asserts
    def test_alarm_state(self):
        print("Assign values (bool, int, string and bool) to the vertical flip attribute...")
        # Testing case: send a bool
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                            json={'attributes': ['alarm_state', False], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['alarm_state', 5], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['alarm_state', 'string'], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                             json={'attributes': ['alarm_state', True], 'cameraMac': self.mac},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()
