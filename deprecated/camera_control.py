import unittest
import requests
import time
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Camera_Control(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting camera_control tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_generate_CCS_event(self):
        input('Connect the camera. Press enter to continue.')
        input("Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        # --------------------------------------------------------------------------------------------------------------

        print()
        print("Performing an initsession")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                             json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        print()
        input('Disconnect the camera. Press enter to continue '
              '(15 seconds delay will be generated for video server updates).')
        time.sleep(15)  # Delay for the update of the camera in the camera control service of the video server

        print("Checking that the mac is NOT listed in cameracontrol video server service")
        req2 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado2 = req2.json()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertNotIn(self.mac, str(resultado2), "Camera mac is listed in the camera control service")

        # --------------------------------------------------------------------------------------------------------------

        print()
        print("Checking in statistics that the camera 'eventclose' is 'CSS'")
        req3 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/statistics/specs?'
                                                            'page=0&size=100&sort=id,desc&search(cameramac:%27' +
                            self.mac + '%27',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))

        resultado3 = req3.json()
        print("Content: " + str(resultado3['content'][0]))

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")
        self.assertEqual('CCS', str(resultado3['content'][0]['eventClose']), "Disconnection does not indicate CCS in"
                                                                             " statistics")

    @unittest.skip("Ready to test")
    def test_disconnect_and_reconnect_camera(self):
        input('Connect the camera. Press enter to continue.')
        input("Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        # --------------------------------------------------------------------------------------------------------------

        print()
        print("Performing an initsession")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                             json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        print()
        input('Disconnect the camera and reconnect it.')
        input("Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req3 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado3 = req3.json()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado3), "Camera mac is not listed in the camera control service")

        # --------------------------------------------------------------------------------------------------------------

        print()
        print("Performing a second initsession")
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                             json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        print()
        print("Checking in statistics that the first initsession is 'CSS' and the second is 'null'")
        req5 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/statistics/specs?'
                                                            'page=0&size=100&sort=id,desc&search(cameramac:%27' +
                            self.mac + '%27',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req5.status_code))

        resultado5 = req5.json()
        print("Content: " + str(resultado5['content'][0]['eventClose']))

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")
        self.assertEqual('CCS', str(resultado5['content'][1]['eventClose']), "Disconnection does not indicate CCS in"
                                                                             " statistics")
        self.assertEqual('None', str(resultado5['content'][0]['eventClose']), "A new session is not indicated as null")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


if __name__ == '__main__':
    unittest.main()
