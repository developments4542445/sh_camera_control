import unittest
import requests
import pygame
import time
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Device_and_Subscriptions(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'
    QR_path = '/Automation_Camera_Control/files_needed/QR-Pompeya.png'

    def open_QR(self):
        # Open image of the QR code
        pygame.init()
        display_width = 500
        display_height = 500
        surface = pygame.display.set_mode((display_width, display_height))
        pygame.display.set_caption('Image')
        display_image = pygame.image.load(self.QR_path)

        surface.fill((255, 255, 255))
        surface.blit(display_image, (0, 0))
        variable = False
        pygame.display.update()
        input('Show QR to the camera. Press enter to continue.')
        pygame.quit()

    def mac_string_change(self, string_mac):
        separator = '%3A'
        string_mac_sep = string_mac[0:2] + separator + string_mac[3:5] + separator + string_mac[6:8] + separator + \
                         string_mac[9:11] + separator + string_mac[12:14] + separator + string_mac[15:17]
        return string_mac_sep

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting Device_and_Subscriptions tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_add_new_camera_not_loaded(self):
        input('Check in CSide page that the camera is not listed. Press enter to continue.')
        input('Perform a factory reset of the camera. Press enter to continue.')
        input('Wait until the camera is up and running. Motor shall stop and the led shall be red and blinking.'
              ' Press enter to continue.')

        self.open_QR()  # Open QR code and displays it

        input("Led shall be green and blinking. Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        print('Add a new camera not previously loaded was performed successfully.')

    @unittest.skip("Ready to test")
    def test_add_new_camera_previously_loaded(self):
        input('Check in CSide page that the camera IS previously listed. Press enter to continue.')
        input('Perform a factory reset of the camera. Press enter to continue.')
        input('Wait until the camera is up and running. Motor shall stop and the led shall be red and blinking.'
              ' Press enter to continue.')

        self.open_QR()  # Open QR code and displays it

        input("Led shall be green and blinking. Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        print('Add a new camera previously loaded was performed successfully.')

    @unittest.skip("Ready to test")
    def test_disconnection_of_camera(self):
        input('Connect the camera. Press enter to continue.')
        input("Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        input('Disconnect the camera. Press enter to continue '
              '(15 seconds delay will be generated for video server updates).')
        time.sleep(15)  # Delay for the update of the camera in the camera control service of the video server

        print("Checking that the mac is NOT listed in cameracontrol video server service")
        req2 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado2 = req2.json()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertNotIn(self.mac, str(resultado2), "Camera mac is listed in the camera control service")

        print('Disconnection of the camera status was performed successfully.')

    @unittest.skip("Ready to test")
    def test_version_sync(self):
        input('Connect the camera. Press enter to continue.')
        input("Wait until camera indicates the advertisement: "
              "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
              " Press enter to continue.")

        print("Checking that the mac is listed in cameracontrol video server service")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertIn(self.mac, str(resultado), "Camera mac is not listed in the camera control service")

        # ---------------------------------------------------------------------------------------------------

        print('Test over the versionsync will be performed.')
        string_mac = self.mac_string_change(self.mac)
        req2 = requests.get('https://' + self.environment +
                            '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' + string_mac,
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking if contains or not the mac specified before
        resultado2 = req2.content

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertNotIn('Camera response timeout expired', str(resultado2),
                         "Fail: Response is not the version of the camera.")

        print('versionsync - API was performed succesfully.')

    # ---------------------------------------------------------------------------------------------------

        req3 = requests.get('https://' + self.environment +
                            '-vs.mirgor.com.ar/cameracontrol/private/versionSync/',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Asserts
        self.assertEqual(404, req3.status_code, "versionsync - void parameter tests was not performed successfully")

        print('versionsync - void parameter tests was performed successfully.')

    # ---------------------------------------------------------------------------------------------------

        bad_string_mac = 'AA%3AFF%3ACC%3AFF%3AEE%3AAG' # String is not OK
        req4 = requests.get('https://' + self.environment +
                            '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' + bad_string_mac,
                            auth=HTTPBasicAuth(self.username, self.password))

        # Asserts
        self.assertEqual(500, req4.status_code,
                         "versionsync - mac value out of range tests was not performed successfully")

        print('versionsync - mac value out of range tests was performed successfully.')

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


# if __name__ == '__main__':
#     unittest.main()
