import unittest
import requests
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Get_Config_Tests(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'


    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting getConfig tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Needs to be implemented in camera")
    def test_get_horiz_flip(self):
        print("Get if the horizontal flip is enabled or disabled...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['horiz_flip'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if resultado['attributes'][0]['value'] is True:
            flag_response = True
        elif resultado['attributes'][0]['value'] is False:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('horiz_flip', resultado['attributes'][0]['attribute'], "Response was not 'horiz_flip'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")
        self.assertTrue(flag_response, "Value in response is not correct")

    @unittest.skip("Needs to be implemented in camera")
    def test_get_vert_flip(self):
        print("Get if the vertical flip is enabled or disabled...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['vert_flip'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if resultado['attributes'][0]['value'] is True:
            flag_response = True
        elif resultado['attributes'][0]['value'] is False:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('vert_flip', resultado['attributes'][0]['attribute'], "Response was not 'vert_flip'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is not correct")
        self.assertTrue(flag_response, "Value in response is not correct")

    @unittest.skip("Ready to test")
    def test_get_img_res(self):
        print("Get the resolution of the image in the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['img_res'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if 'sd' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'hd' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'fhd' in resultado['attributes'][0]['value']:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('img_res', resultado['attributes'][0]['attribute'], "Response was not 'img_res'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Response is not sd, hd or fhd")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    def test_get_motion_sensitivity(self):
        print("Get the motion detector sensitivity of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['motion_sensitivity'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if 'low' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'mid' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'high' in resultado['attributes'][0]['value']:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('motion_sensitivity', resultado['attributes'][0]['attribute'],
                         "Response was not 'motion_sensitivity'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Response is not low, mid or high")

    @unittest.skip("Needs to be implemented in camera")
    # add check to the value got by the API
    def test_get_videoclip_block_zone(self):
        print("Get the video clip block zone of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['videoclip_block_zone'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('motion_sensitivity', resultado['attributes'][0]['attribute'],
                         "Response was not 'videoclip_block_zone'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")

    @unittest.skip("Needs to be implemented in camera")
    def test_get_sound_threshold(self):
        print("Get the sound threshold of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['sound_threshold'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if 'low' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'mid' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'high' in resultado['attributes'][0]['value']:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('sound_threshold', resultado['attributes'][0]['attribute'],
                         "Response was not 'sound_threshold'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Response is not low, mid or high")

    @unittest.skip("Needs to be implemented in camera")
    def test_get_day_night_threshold(self):
        print("Get the day / night threshold of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['day_night_threshold'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if 'low' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'mid' in resultado['attributes'][0]['value']:
            flag_response = True
        elif 'high' in resultado['attributes'][0]['value']:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('day_night_threshold', resultado['attributes'][0]['attribute'],
                         "Response was not 'day_night_threshold'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Response is not low, mid or high")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    def test_get_day_night_automatic(self):
        print("Get the day / night automatic shift of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['day_night_automatic'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if resultado['attributes'][0]['value'] is True:
            flag_response = True
        elif resultado['attributes'][0]['value'] is False:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('day_night_automatic', resultado['attributes'][0]['attribute'],
                         "Response was not 'day_night_automatic'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Value in response is not correct")

    @unittest.skip("Needs to be implemented in camera")
    # add check to the value got by the API
    def test_get_point_of_interest(self):
        print("Get the point of interest of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['point_of_interest'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('point_of_interest', resultado['attributes'][0]['attribute'],
                         "Response was not 'point_of_interest'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")

    @unittest.skip("Needs to be implemented in camera")
    # add check to the value got by the API
    def test_get_home_position(self):
        print("Get the home position of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['home_position'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('home_position', resultado['attributes'][0]['attribute'],
                         "Response was not 'home_position'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    def test_get_alarm_state(self):
        print("Get the state of the alarm of the camera...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                            json={'attributes': ['alarm_state'], 'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        if resultado['attributes'][0]['value'] is True:
            flag_response = True
        elif resultado['attributes'][0]['value'] is False:
            flag_response = True
        else:
            flag_response = False

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('alarm_state', resultado['attributes'][0]['attribute'],
                         "Response was not 'alarm_state'")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "Value in response is null")
        self.assertTrue(flag_response, "Value in response is not correct")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()
