import unittest
import requests
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Set_Command_Tests(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting setCommand tests...")
        print("-----------------------------------------")
        print()
        print()

    @unittest.skip("Needs to be implemented in camera")
    def test_set_bw_measure(self):
        print("Assign values (null, int, string and bool) to the WiFi bandwidth measurement attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None, 'cameraMac': self.mac, 'command': 'bw_measure'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 100, 'cameraMac': self.mac, 'command': 'bw_measure'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'bw_measure'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'bw_measure'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_panning(self):
        # Testing if true
        print("Assign a boolean value (true and false) to the panning attribute...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': True, 'cameraMac': self.mac, 'command': 'panning'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing if false
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': False, 'cameraMac': self.mac, 'command': 'panning'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera. Define the minimum and maximum tilt_step")
    def test_set_int_tilt_step(self):
        MAX_V_STEPS = 200

        # Testing if less than -MAX_V_STEPS
        print("Assign values (less, equal and greater) for the tilt step attribute...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': -MAX_V_STEPS, 'cameraMac': self.mac, 'command': 'tilt_step'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing if more than +MAX_V_STEPS
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': MAX_V_STEPS, 'cameraMac': self.mac, 'command': 'tilt_step'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing if -MAX_V_STEPS < sent < +MAX_V_STEPS
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 200, 'cameraMac': self.mac, 'command': 'tilt_step'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(200, req3.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera. Define the minimum and maximum pan_step")
    def test_set_int_pan_step(self):
        MAX_H_STEPS = 200

        # Testing if less than -MAX_H_STEPS
        print("Assign values (less, equal and greater) for the pan step attribute...")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': -MAX_H_STEPS, 'cameraMac': self.mac, 'command': 'pan_step'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing if more than +MAX_H_STEPS
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': MAX_H_STEPS, 'cameraMac': self.mac, 'command': 'pan_step'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing if -MAX_H_STEPS < sent < +MAX_H_STEPS
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 200, 'cameraMac': self.mac, 'command': 'pan_step'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(200, req3.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

    @unittest.skip("Check if asserts are OK. Update to version 0.1.18")
    def test_set_go_to_home(self):
        print("Assign values (null, int, string and bool) to the go to home attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None, 'cameraMac': self.mac, 'command': 'go_to_home'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 100, 'cameraMac': self.mac, 'command': 'go_to_home'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'go_to_home'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'go_to_home'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_go_to_poi(self):
        print("Assign values (int, null, string and bool) to the point of interest measurement attribute...")
        # Testing case: send an integer
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': 5,  'cameraMac': self.mac, 'command': 'go_to_poi'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a null
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': None, 'cameraMac': self.mac, 'command': 'go_to_poi'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'go_to_poi'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'go_to_poi'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_factory_reset(self):
        print("Assign values (null, int, string and bool) for the factory reset attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None,  'cameraMac': self.mac, 'command': 'factory_reset'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 5, 'cameraMac': self.mac, 'command': 'factory_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'factory_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'factory_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_warm_reset(self):
        print("Assign values (null, int, string and bool) for the warm_reset attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None,  'cameraMac': self.mac, 'command': 'warm_reset'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 5, 'cameraMac': self.mac, 'command': 'warm_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'warm_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'warm_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_cold_reset(self):
        print("Assign values (null, int, string and bool) to the cold reset attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None,  'cameraMac': self.mac, 'command': 'cold_reset'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 5, 'cameraMac': self.mac, 'command': 'cold_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'cold_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'cold_reset'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @unittest.skip("Needs to be implemented in camera")
    def test_set_trigger_videoclips(self):
        print("Assign values (null, int, string and bool) to the trigger videoclips attribute...")
        # Testing case: send a null
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                            json={'argument': None,  'cameraMac': self.mac, 'command': 'trigger_videoclips'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        # Checking by content
        resultado = req.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send an integer
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 5, 'cameraMac': self.mac, 'command': 'trigger_videoclips'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))

        # Checking by content
        resultado2 = req2.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a string
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': 'string', 'cameraMac': self.mac, 'command': 'trigger_videoclips'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))

        # Checking by content
        resultado3 = req3.json()

        # ---------------------------------------------------------------------------------------------------

        # Testing case: send a bool
        req4 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                             json={'argument': True, 'cameraMac': self.mac, 'command': 'trigger_videoclips'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req4.status_code))
        print("Content: " + str(req4.content))

        # Checking by content
        resultado4 = req4.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual('to_be_defined', resultado['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req2.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado2['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado2['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req3.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado3['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado3['attributes'][0]['value'], "TBD")

        self.assertEqual(409, req4.status_code, "status_code was not 409")
        self.assertEqual('to_be_defined', resultado4['attributes'][0]['attribute'], "TBD")
        self.assertIsNotNone(resultado4['attributes'][0]['value'], "TBD")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()
