import unittest
import HtmlTestRunner

# Tests to perform
from deprecated import getStatus_APIs, setCommand_APIs, getConfig_APIs, setConfig_APIs, camera_control, \
    device_and_subscriptions

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTests(loader.loadTestsFromModule(getConfig_APIs))
    suite.addTests(loader.loadTestsFromModule(getStatus_APIs))
    suite.addTests(loader.loadTestsFromModule(setCommand_APIs))
    suite.addTests(loader.loadTestsFromModule(setConfig_APIs))
    suite.addTests(loader.loadTestsFromModule(device_and_subscriptions))
    suite.addTests(loader.loadTestsFromModule(camera_control))
    # runner = unittest.TextTestRunner(verbosity=3)
    runner = HtmlTestRunner.\
        HTMLTestRunner(verbosity=3, report_title='Automation Camera Control Service', descriptions='QA Automation',
                       report_name='Automatic Camera Control Service', output='reports', open_in_browser=True)
    results = runner.run(suite)
