# To run this, we have to write:
# behave features/devices_subscriptions.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: X minutes X seconds
@every_scenario
Feature: CameraControl Service - Devices and Subscriptions Tools

  @semi_automation @finished @happy
  Scenario: Adding a new Camera to the Server
    Given Perform a factory reset to the SmartCam
    Then Initializing the SmartCam after factory reset - Show QR code
    And Check that the SmartCam is listed in the video server

  @automation @finished @happy
  Scenario: versionSync - Correct Answer
    Given Perform a versionSync
    Then Check if asserts are correct - version_sync - correct answer

  @automation @finished @rainy
  Scenario: versionSync - MAC with special characters
    Given Perform a versionSync - MAC with special characters
    Then Check if asserts are correct - version_sync - MAC with special characters

  @automation @finished @rainy
  Scenario: versionSync - MAC with more than 12 digits
    Given Perform a versionSync - MAC with more than 12 digits
    Then Check if asserts are correct - version_sync - MAC with more than 12 digits

  @automation @finished @rainy
  Scenario: versionSync - void
    Given Perform a versionSync - void
    Then Check if asserts are correct - version_sync - void
