# To run this, we have to write:
# behave features/getConfig_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 2.673 seconds
@every_scenario
Feature: CameraControl Service getConfig APIs Tests

  @automation @finished @happy
  Scenario: Get the config of attributes - horiz_flip
    Given get the config attributes - horiz_flip
    Then check if asserts are correct - horiz_flip

  @automation @finished @happy
  Scenario: Get the config of attributes - vert_flip
    Given get the config attributes - vert_flip
    Then check if asserts are correct - vert_flip

  @automation @finished @rainy
  Scenario: Get the config of attributes - img_res
    Given get the config attributes - img_res
    Then check if asserts are correct - img_res

  @automation @finished @rainy
  Scenario: Get the config of attributes - motion_sensitivity
    Given get the config attributes - motion_sensitivity
    Then check if asserts are correct - motion_sensitivity

  @automation @finished @rainy
  Scenario: Get the config of attributes - videoclip_block_zone
    Given get the config attributes - videoclip_block_zone
    Then check if asserts are correct - videoclip_block_zone

  @automation @not_implemented_in_camera @happy
  Scenario: Get the config of attributes - sound_threshold
    Given get the config attributes - sound_threshold
    Then check if asserts are correct - sound_threshold

  @automation @not_implemented_in_camera @happy
  Scenario: Get the config of attributes - day_night_threshold
    Given get the config attributes - day_night_threshold
    Then check if asserts are correct - day_night_threshold

  @automation @not_implemented_in_camera @happy @check_if_there_is_a_bug_here
  Scenario: Get the config of attributes - day_night_automatic
    Given get the config attributes - day_night_automatic
    Then check if asserts are correct - day_night_automatic

  @automation @finished @happy @bug_in_videoserver_wait_until_it_is_resolved_TEVIDSHOME_482
  Scenario: Get the config of attributes - point_of_interest
    Given get the config attributes - point_of_interest
    Then check if asserts are correct - point_of_interest

  @automation @finished @happy
  Scenario: Get the config of attributes - home_position
    Given get the config attributes - home_position
    Then check if asserts are correct - home_position

  @automation @finished @happy
  Scenario: Get the config of attributes - alarm_state
    Given get the config attributes - alarm_state
    Then check if asserts are correct - alarm_state

