# To run this, we have to write:
# behave features/setCommand_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 3.225 seconds (only tests in finished)
@every_scenario
Feature: CameraControl Service setCommand APIs Tests

  @automation @not_implemented_in_camara @happy @rainy
  Scenario: Assign a null value to the WiFi bandwidth measurement attribute
    Given set command for bw_measure - null - OK - bw_measure
    And set command for bw_measure - integer - wrong - bw_measure
    And set command for bw_measure - string - wrong - bw_measure
    And set command for bw_measure - bool - wrong - bw_measure
    Then check if asserts are correct - bw_measure

  @automation @finished @happy @rainy
  Scenario: Assign an boolean value to the panning attribute
    Given set command for panning - null - wrong - panning
    And set command for panning - integer - wrong - panning
    And set command for panning - string - wrong - panning
    And set command for panning - bool - OK - panning
    Then check if asserts are correct - panning

  @automation @finished @happy @rainy
  Scenario: Assign in-range values for the tilt step attribute
    Given set command for tilt step attribute - null - wrong - tilt_step
    And set command for tilt step attribute - minimum_value - OK - tilt_step
    And set command for tilt step attribute - intermediate_value - OK - tilt_step
    And set command for tilt step attribute - maximum_value - OK - tilt_step
    And set command for tilt step attribute - string - wrong - tilt_step
    And set command for tilt step attribute - bool - wrong - tilt_step
    Then check if asserts are correct - tilt_step

  @automation @finished @rainy
  Scenario: Assign more than MAX_V_STEPS in the tilt step attribute
    Given set command for tilt step attribute - 1001 - wrong - tilt_step
    Then check if asserts are correct - 1001 - tilt_step

  @automation @finished @rainy
  Scenario: Assign less than -MAX_V_STEPS in the tilt step attribute
    Given set command for tilt step attribute - -1001 - wrong - tilt_step
    Then check if asserts are correct - -1001 - tilt_step

  @automation @finished @happy @rainy
  Scenario: Assign in-range values for the panning step attribute
    Given set command for panning step attribute - null - wrong - pan_step
    And set command for panning step attribute - minimum_value - OK - pan_step
    And set command for panning step attribute - intermediate_value - OK - pan_step
    And set command for panning step attribute - maximum_value - OK - pan_step
    And set command for panning step attribute - string - wrong - pan_step
    And set command for panning step attribute - bool - wrong - pan_step
    Then check if asserts are correct - pan_step

  @automation @finished @rainy
  Scenario: Assign more than MAX_H_STEPS in the pan step attribute
    Given set command for pan step attribute - 1001 - wrong - pan_step
    Then check if asserts are correct - 1001 - pan_step

  @automation @finished @rainy
  Scenario: Assign less than -MAX_H_STEPS in the pan step attribute
    Given set command for pan step attribute - -1001 - wrong - pan_step
    Then check if asserts are correct - -1001 - pan_step

  @automation @finished @happy @rainy
  Scenario: Assign a null value in Move to Home attribute
    Given set command for go_to_home - null - OK - go_to_home
    And set command for go_to_home - integer - wrong - go_to_home
    And set command for go_to_home - string - wrong - go_to_home
    And set command for go_to_home - bool - wrong - go_to_home
    Then check if asserts are correct - go_to_home

  @automation @finished @happy @rainy
  Scenario: Assign in-range values in Move to a POI attribute
    Given set command for go_to_poi - null - wrong - go_to_poi
    And set command for go_to_poi - minimum_value - OK - go_to_poi
    And wait time to move to the position selected - go_to_poi
    And set command for go_to_poi - intermediate_value - OK - go_to_poi
    And wait time to move to the position selected - go_to_poi
    And set command for go_to_poi - maximum_value - OK - go_to_poi
    And wait time to move to the position selected - go_to_poi
    And set command for go_to_poi - string - wrong - go_to_poi
    And set command for go_to_poi - bool - wrong - go_to_poi
    Then check if asserts are correct - go_to_poi

  @automation @finished @rainy
  Scenario: Assign less than minimum_value in the go to POI attribute
    Given set command for go_to_poi - 0 - wrong - go_to_poi
    Then check if asserts are correct - 0 - go_to_poi

  @automation @finished @rainy
  Scenario: Assign more than maximum_value in the go to POI attribute
    Given set command for go_to_poi - 6 - wrong - go_to_poi
    Then check if asserts are correct - 6 - go_to_poi

  @automation @not_implemented_in_camara @happy @rainy
  Scenario: Assign a null value in Factory reset attribute
    Given set command for factory_reset - null - OK - factory_reset
    And set command for factory_reset - integer - wrong - factory_reset
    And set command for factory_reset - string - wrong - factory_reset
    And set command for factory_reset - bool - wrong - factory_reset
    Then check if asserts are correct - factory_reset

  @automation @finished @happy @rainy
  Scenario: Assign a null value in Warm reset attribute
    Given set command for warm_reset - null - OK - warm_reset
    And set command for warm_reset - integer - wrong - warm_reset
    And set command for warm_reset - string - wrong - warm_reset
    And set command for warm_reset - bool - wrong - warm_reset
    Then check if asserts are correct - warm_reset

  @automation @finished @happy @rainy
  Scenario: Assign a null value in Cold reset attribute
    Given set command for cold_reset - null - OK - cold_reset
    And set command for cold_reset - integer - wrong - cold_reset
    And set command for cold_reset - string - wrong - cold_reset
    And set command for cold_reset - bool - wrong - cold_reset
    Then check if asserts are correct - cold_reset

  @automation @not_implemented_in_camara @happy @rainy
  Scenario: Assign a bool value in Trigger the Video clip Capture attribute
    Given set command for trigger_videoclip - bool - OK - trigger_videoclip
    And set command for trigger_videoclip - null - wrong - trigger_videoclip
    And set command for trigger_videoclip - integer - wrong - trigger_videoclip
    And set command for trigger_videoclip - string - wrong - trigger_videoclip
    Then check if asserts are correct - trigger_videoclip

  @automation @finished @rainy
  Scenario: Perform a tilt_step during a panning
    Given set command for panning - true
    Then Perform a tilt_step while panning is on
    And finish panning
    And check if asserts are correct - panning and tilt_step

  @automation @finished @rainy
  Scenario: Perform a pan_step during a panning
    Given set command for panning - true
    Then Perform a pan_step while panning is on
    And finish panning
    And check if asserts are correct - panning and pan_step

  @automation @finished @rainy
  Scenario: Get the current_position during a panning
    Given set command for panning - true
    Then Perform a current_position while panning is on
    And finish panning
    And check if asserts are correct - panning and current_position

  @automation @finished @rainy
  Scenario: Set a home_position during a panning
    Given set command for panning - true
    Then Perform a home_position while panning is on
    And finish panning
    And check if asserts are correct - panning and home_position

  @automation @finished @rainy
  Scenario: Perform a go_to_home during a panning
    Given set command for panning - true
    Then Perform a go_to_home while panning is on
    And finish panning
    And check if asserts are correct - panning and go_to_home

  @automation @finished @rainy
  Scenario: Perform a go_to_poi during a panning
    Given set command for panning - true
    Then Perform a go_to_poi while panning is on
    And finish panning
    And check if asserts are correct - panning and go_to_poi

  @automation @finished @rainy
  Scenario: Set a point_of_interest during a panning
    Given set command for panning - true
    Then Perform a point_of_interest while panning is on
    And finish panning
    And check if asserts are correct - panning and point_of_interest

  @semi_automation @finished @rainy
  Scenario: Go to point_of_interest after a panning
    Given set command for panning - true
    When delay for the panning - 1 minute
    Then finish panning
    And set command go_to_poi - position 1
    And check if position and asserts are correct - go_to_poi after panning

  @semi_automation @finished @rainy
  Scenario: Go to home_position after a panning
    Given set command for panning - true
    When delay for the panning - 1 minute
    Then finish panning
    And set command go_to_home after panning
    And check if position and asserts are correct - go_to_home after panning
