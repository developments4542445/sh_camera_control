# To run this, we have to write:
# behave features/setConfig_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 3.874 seconds (@finished Scenarios)
@every_scenario
Feature: CameraControl Service setConfig APIs Tests

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - horiz_flip
    Given configure the camera - null - wrong - horiz_flip
    And configure the camera - int - wrong - horiz_flip
    And configure the camera - string - wrong - horiz_flip
    And configure the camera - bool - OK - horiz_flip
    Then check if configure asserts are correct - horiz_flip

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - vert_flip
    Given configure the camera - null - wrong - vert_flip
    And configure the camera - int - wrong - vert_flip
    And configure the camera - string - wrong - vert_flip
    And configure the camera - bool - OK - vert_flip
    Then check if configure asserts are correct - vert_flip

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - img_res
    Given configure the camera - null - wrong - img_res
    And configure the camera - int - wrong - img_res
    And configure the camera - string - OK - img_res
    And configure the camera - bool - wrong - img_res
    Then check if configure asserts are correct - img_res

  @automation @finished @rainy
  Scenario: Set the config of attributes - img_res - wrong string
    Given configure the camera - wrong string - wrong - img_res
    Then check if configure asserts are correct - wrong string - img_res

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - motion_sensitivity
    Given configure the camera - null - wrong - motion_sensitivity
    And configure the camera - int - wrong - motion_sensitivity
    And configure the camera - string - OK - motion_sensitivity
    And configure the camera - bool - wrong - motion_sensitivity
    Then check if configure asserts are correct - motion_sensitivity

  @automation @finished @rainy
  Scenario: Set the config of attributes - motion_sensitivity - wrong string
    Given configure the camera - wrong string - wrong - motion_sensitivity
    Then check if configure asserts are correct - wrong string - motion_sensitivity

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - videoclip_block_zone
    Given configure the camera - null - wrong - videoclip_block_zone
    And configure the camera - int - wrong - videoclip_block_zone
    And configure the camera - string - wrong - videoclip_block_zone
    And configure the camera - bool - wrong - videoclip_block_zone
    And configure the camera - JSON - OK - videoclip_block_zone
    And configure the camera - JSON - wrong x - videoclip_block_zone
    And configure the camera - JSON - wrong y - videoclip_block_zone
    Then check if configure asserts are correct - videoclip_block_zone

  @automation @not_implemented_in_camera @happy @rainy
  Scenario: Set the config of attributes - sound_threshold
    Given configure the camera - null - wrong - sound_threshold
    And configure the camera - int - wrong - sound_threshold
    And configure the camera - string - OK - sound_threshold
    And configure the camera - bool - wrong - sound_threshold
    Then check if configure asserts are correct - sound_threshold

  @automation @finished @rainy
  Scenario: Set the config of attributes - sound_threshold - wrong string
    Given configure the camera - wrong string - wrong - sound_threshold
    Then check if configure asserts are correct - wrong string - sound_threshold

  @automation @not_implemented_in_camera @happy @rainy
  Scenario: Set the config of attributes - day_night_threshold
    Given configure the camera - null - wrong - day_night_threshold
    And configure the camera - int - wrong - day_night_threshold
    And configure the camera - string - OK - day_night_threshold
    And configure the camera - bool - wrong - day_night_threshold
    Then check if configure asserts are correct - day_night_threshold

  @automation @finished @rainy
  Scenario: Set the config of attributes - day_night_threshold - wrong string
    Given configure the camera - wrong string - wrong - day_night_threshold
    Then check if configure asserts are correct - wrong string - day_night_threshold

  @automation @not_implemented_in_camera @happy @rainy
  Scenario: Set the config of attributes - day_night_automatic
    Given configure the camera - null - wrong - day_night_automatic
    And configure the camera - int - wrong - day_night_automatic
    And configure the camera - string - wrong - day_night_automatic
    And configure the camera - bool - OK - day_night_automatic
    Then check if configure asserts are correct - day_night_automatic

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - point_of_interest
    Given configure the camera - null - wrong - point_of_interest
    And configure the camera - int - OK - point_of_interest
    And configure the camera - string - wrong - point_of_interest
    And configure the camera - bool - wrong - point_of_interest
    Then check if configure asserts are correct - point_of_interest

  @automation @finished @rainy
  Scenario: Set the config of attributes - point_of_interest - wrong value
    Given configure the camera - value 0 - wrong - point_of_interest
    And configure the camera - value 6 - wrong - point_of_interest
    Then check if configure asserts are correct - wrong string - point_of_interest

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - home_position
    Given configure the camera - null - OK - home_position
    And configure the camera - int - wrong - home_position
    And configure the camera - string - wrong - home_position
    And configure the camera - bool - wrong - home_position
    Then check if configure asserts are correct - home_position

  @automation @finished @happy @rainy
  Scenario: Set the config of attributes - alarm_state
    Given configure the camera - null - wrong - alarm_state
    And configure the camera - int - wrong - alarm_state
    And configure the camera - string - wrong - alarm_state
    And configure the camera - bool - OK - alarm_state
    Then check if configure asserts are correct - alarm_state

  @automation @finished @rainy
  Scenario: Configure the camera by entering two equal attributes with different values - Duplicated element
    Given configure the camera - first and second value - img_res
    Then check if configure asserts are correct - duplicated element

  @automation @finished @rainy
  Scenario: Configure the camera by entering two attributes with the same values - Duplicated element
    Given configure the camera - equal values - img_res
    Then check if configure asserts are correct - equal values
