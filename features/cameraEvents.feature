# To run this, we have to write :
# behave features/cameraEvents.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 4 minutes 3.062 seconds
@every_scenario
Feature: CameraControl Service Camera Events

  @semi_automation @finished @happy
  Scenario: Generate a CCS event
    Given Initializing the SmartCam
    Then Checking that SmartCam is running
    And Perform an initSession
    And Disconnect the SmartCam
    And Checking that SmartCam is not listed
    And Checking that session is closed due to CCS

  @semi_automation @finished @happy
  Scenario: Disconnect and Reconnect a SmartCam
    Given Initializing the SmartCam
    Then Checking that SmartCam is running
    And Perform an initSession
    And Disconnect and reconnect the SmartCam
    And Checking that SmartCam is running
    And Perform an initSession
    And Wait 120 seconds after initialization
    And Checking that SmartCam is running
    And Checking both initSession states
