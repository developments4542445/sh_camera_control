# To run this, we have to write:
# behave features/getStatus_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 2.682 seconds
@every_scenario
Feature: CameraControl Service getStatus APIs Tests

  @automation @finished @happy
  Scenario: Get the status of wifi level of a camera
    Given get the status of the wifi level of a camera - wifi_level
    Then check if asserts are correct - wifi_level

  @automation @finished @happy
  Scenario: Get the status of firmware version of a camera
    Given get the status of the firmware version of a camera - fw_version
    Then check if asserts are correct - fw_version

  @automation @not_implemented_in_camera @happy
  Scenario: Get the status of WiFi bandwidth of a camera
    Given get the status of WiFi bandwidth of a camera - wifi_bw
    Then check if asserts are correct - wifi_bw

  @automation @finished @happy
  Scenario: Get the status and current position of a camera
    Given get the status and current position of a camera - current_position
    Then check if asserts are correct - current_position

  @automation @not_implemented_in_camera @happy
  Scenario: Get the status of uSD-card presence of a camera
    Given get the status of uSD-card presence of a camera - usd_presence
    Then check if asserts are correct - usd_presence

  @automation @finished @rainy
  Scenario: Get the status of a non existent attribute
    Given get the status of a non existent attribute
    Then check if asserts are correct - non_existent_attribute
