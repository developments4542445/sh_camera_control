# Perform tests over the devices_subscriptions.feature

from behave import *
from my_functions_class import *
import time
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'


@given(u'Perform a factory reset to the SmartCam')
def step_impl(context):
    print()
    input('Perform a factory reset of the camera. Press enter to continue.')
    input('Wait until the camera is up and running. Motor shall stop and the led shall be red and blinking.'
          ' Press enter to continue.')

    test_finished()


@then(u'Initializing the SmartCam after factory reset - Show QR code')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.open_qr()
    input("Led shall be green and blinking. Wait until camera indicates the advertisement: "
          "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
          " Press enter to continue.")

    test_finished()


@then(u'Check that the SmartCam is listed in the video server')
def step_impl(context):
    print()
    context.my_CCS.all_camera()

    # Asserts
    assert 200 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)
    assert context.my_CCS.mac in str(context.my_CCS.req.json()),\
        "Camera mac is not listed in the camera control service"

    test_finished()


@given(u'Perform a versionSync')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.perform_version_sync()

    test_finished()


@then(u'Check if asserts are correct - version_sync - correct answer')
def step_impl(context):
    print()

    assert 200 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'Perform a versionSync - MAC with special characters')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)

    print('Perform a versionSync.')
    string_mac = context.my_CCS.mac_string_change()
    context.my_CCS.req = requests.get('https://' + context.my_CCS.environment +
                                      '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' +
                                      'AA%3ABB%3ACC%3ADD%3AEE%3A5&',
                                      auth=HTTPBasicAuth(context.my_CCS.username, context.my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.my_CCS.req.status_code))
    print("Content: " + str(context.my_CCS.req.content))

    test_finished()


@then(u'Check if asserts are correct - version_sync - MAC with special characters')
def step_impl(context):
    print()

    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'Perform a versionSync - MAC with more than 12 digits')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)

    print('Perform a versionSync.')
    string_mac = context.my_CCS.mac_string_change()
    context.my_CCS.req = requests.get('https://' + context.my_CCS.environment +
                                      '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' + string_mac +
                                      '%3A' + '12',
                                      auth=HTTPBasicAuth(context.my_CCS.username, context.my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.my_CCS.req.status_code))
    print("Content: " + str(context.my_CCS.req.content))

    test_finished()


@then(u'Check if asserts are correct - version_sync - MAC with more than 12 digits')
def step_impl(context):
    print()

    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'Perform a versionSync - MAC with Values out of range')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)

    print('Perform a versionSync.')
    string_mac = context.my_CCS.mac_string_change()
    context.my_CCS.req = requests.get('https://' + context.my_CCS.environment +
                                      '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' + string_mac +
                                      '%3A' + '12',
                                      auth=HTTPBasicAuth(context.my_CCS.username, context.my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.my_CCS.req.status_code))
    print("Content: " + str(context.my_CCS.req.content))

    test_finished()


@then(u'Check if asserts are correct - version_sync - MAC with Values out of range')
def step_impl(context):
    print()

    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'Perform a versionSync - void')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)

    print('Perform a versionSync.')
    string_mac = context.my_CCS.mac_string_change()
    context.my_CCS.req = requests.get('https://' + context.my_CCS.environment +
                                      '-vs.mirgor.com.ar/cameracontrol/private/versionSync/',
                                      auth=HTTPBasicAuth(context.my_CCS.username, context.my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.my_CCS.req.status_code))
    print("Content: " + str(context.my_CCS.req.content))

    test_finished()


@then(u'Check if asserts are correct - version_sync - void')
def step_impl(context):
    print()

    assert 404 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()
