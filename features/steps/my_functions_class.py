# my_functions_class.py

import requests
import pygame
from requests.auth import HTTPBasicAuth


class CameraControlService:
    req = 'request'  # for json elements

    def __init__(self, mac='AA:BB:CC:DD:EE:53', environment='ist', username='mirgor', password='wayna!'):
        self.mac = mac
        self.environment = environment
        self.username = username
        self.password = password

    def all_camera(self):
        print()
        print("Checking that the mac is listed in cameracontrol video server service")
        self.req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/private/allCamera',
                                auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def get_config(self, attributes):
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/getConfig',
                                 json={'attributes': [attributes], 'cameraMac': self.mac},
                                 auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def mac_string_change(self):
        separator = '%3A'
        string_mac_sep = self.mac[0:2] + separator + self.mac[3:5] + separator + self.mac[6:8] + separator +\
                         self.mac[9:11] + separator + self.mac[12:14] + separator + self.mac[15:17]
        return string_mac_sep

    def open_qr(self):
        QR_path = '/home/fcapdeville/PycharmProjects/Automation_CameraControlService/files_needed/QR-Pompeya.png'
        # Open image of the QR code
        pygame.init()
        display_width = 500
        display_height = 500
        surface = pygame.display.set_mode((display_width, display_height))
        pygame.display.set_caption('Image')
        display_image = pygame.image.load(QR_path)

        surface.fill((255, 255, 255))
        surface.blit(display_image, (0, 0))
        pygame.display.update()
        input('Show QR to the camera. Press enter to continue.')
        pygame.quit()

    def perform_init_session(self):
        print("Performing an initSession")
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                                 json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                                 auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def perform_statistics_specs(self):
        print("Performing a statistics/specs")
        self.req = requests.get('https://' + self.environment +
                                '-vs.mirgor.com.ar/livestream/statistics/specs?page=0&size=100&sort=id,'
                                'desc&search=(cameraMac:%27' + self.mac + '%27)',
                                auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))

    def perform_version_sync(self):
        print('Perform a versionSync.')
        string_mac = self.mac_string_change()
        self.req = requests.get('https://' + self.environment +
                                '-vs.mirgor.com.ar/cameracontrol/private/versionSync/' + string_mac,
                                auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def set_command(self, element, command):
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                                 json={'argument': element, 'cameraMac': self.mac, 'command': command},
                                 auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def set_config(self, attribute, value):
        print()
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                                 json={'attributes': [{'attribute': attribute, 'value': value}],
                                       'cameraMac': self.mac},
                                 auth=HTTPBasicAuth(self.username, self.password))
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def set_config_json(self, attribute, valuex, valuey):
        print()
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                                 json={'attributes': [{'attribute': attribute, 'value': [{'x': valuex, 'y': valuey}]}],
                                       'cameraMac': self.mac},
                                 auth=HTTPBasicAuth(self.username, self.password))
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))


def test_finished():
    print()
    print('Last print do not work properly')
    pass
