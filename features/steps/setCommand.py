# Perform tests over the setCommand_APIs.feature

from behave import *
from my_functions_class import *
import time
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'


@given(u'set command for bw_measure - null - OK - bw_measure')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'bw_measure')

    test_finished()


@given(u'set command for bw_measure - integer - wrong - bw_measure')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'bw_measure')

    test_finished()


@given(u'set command for bw_measure - string - wrong - bw_measure')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'bw_measure')

    test_finished()


@given(u'set command for bw_measure - bool - wrong - bw_measure')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(True, 'bw_measure')

    test_finished()


@then(u'check if asserts are correct - bw_measure')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'to_be_defined' == result1['attributes'][0]['attribute'], "Define developer responses"
    assert result1['attributes'][0]['value'] is not None, "Define developer responses"

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@given(u'set command for panning - null - wrong - panning')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'panning')

    test_finished()


@given(u'set command for panning - integer - wrong - panning')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'panning')

    test_finished()


@given(u'set command for panning - string - wrong - panning')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'panning')

    test_finished()


@given(u'set command for panning - bool - OK - panning')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(False, 'panning')

    test_finished()


@then(u'check if asserts are correct - panning')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert None is result4['attributes'], "Panning is not null: " + str(result4['attributes'])

    test_finished()


@given(u'set command for panning - true')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(True, 'panning')

    test_finished()


@then(u'Perform a tilt_step while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(50, 'tilt_step')

    test_finished()


@then(u'finish panning')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command(False, 'panning')

    test_finished()


@then(u'check if asserts are correct - panning and tilt_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Tried to change pan while motor is panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@then(u'Perform a pan_step while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(50, 'pan_step')

    test_finished()


@then(u'check if asserts are correct - panning and pan_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Tried to change pan while motor is panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@given(u'set command for tilt step attribute - null - wrong - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'tilt_step')

    test_finished()


@given(u'set command for tilt step attribute - minimum_value - OK - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(-22, 'tilt_step')

    test_finished()


@given(u'set command for tilt step attribute - intermediate_value - OK - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command(0, 'tilt_step')

    test_finished()


@given(u'set command for tilt step attribute - maximum_value - OK - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(54, 'tilt_step')

    test_finished()


@given(u'set command for tilt step attribute - string - wrong - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_5 = CameraControlService(mac, environment, username, password)
    context.my_CCS_5.set_command('string', 'tilt_step')

    test_finished()


@given(u'set command for tilt step attribute - bool - wrong - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_6 = CameraControlService(mac, environment, username, password)
    context.my_CCS_6.set_command(False, 'tilt_step')

    test_finished()


@then(u'check if asserts are correct - tilt_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()
    result5 = context.my_CCS_5.req.json()
    result6 = context.my_CCS_6.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    assert 200 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert None is result2['attributes'], "tilt_step is not null: " + str(result2['attributes'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert None is result3['attributes'], "tilt_step is not null: " + str(result3['attributes'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert None is result4['attributes'], "tilt_step is not null: " + str(result4['attributes'])

    assert 409 == context.my_CCS_5.req.status_code, "ERROR. status_code: " + str(context.my_CCS_5.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_5.req.content)
    assert 'Fail validation attributes' == str(result5['message']), "Error in response: " + str(result5['message'])

    assert 409 == context.my_CCS_6.req.status_code, "ERROR. status_code: " + str(context.my_CCS_6.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_6.req.content)
    assert 'Fail validation attributes' == str(result6['message']), "Error in response: " + str(result6['message'])

    test_finished()


@given(u'set command for tilt step attribute - 1001 - wrong - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(1001, 'tilt_step')

    test_finished()


@then(u'check if asserts are correct - 1001 - tilt_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for tilt step attribute - -1001 - wrong - tilt_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(-1001, 'tilt_step')

    test_finished()


@then(u'check if asserts are correct - -1001 - tilt_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for panning step attribute - null - wrong - pan_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'pan_step')

    test_finished()


@given(u'set command for panning step attribute - minimum_value - OK - pan_step')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(-174, 'pan_step')

    test_finished()


@given(u'set command for panning step attribute - intermediate_value - OK - pan_step')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command(20, 'pan_step')

    test_finished()


@given(u'set command for panning step attribute - maximum_value - OK - pan_step')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(174, 'pan_step')

    test_finished()


@given(u'set command for panning step attribute - string - wrong - pan_step')
def step_impl(context):
    print()
    context.my_CCS_5 = CameraControlService(mac, environment, username, password)
    context.my_CCS_5.set_command('string', 'pan_step')

    test_finished()


@given(u'set command for panning step attribute - bool - wrong - pan_step')
def step_impl(context):
    print()
    context.my_CCS_6 = CameraControlService(mac, environment, username, password)
    context.my_CCS_6.set_command(False, 'pan_step')

    test_finished()


@then(u'check if asserts are correct - pan_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()
    result5 = context.my_CCS_5.req.json()
    result6 = context.my_CCS_6.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    assert 200 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert None is result2['attributes'], "pan_step is not null: " + str(result2['attributes'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert None is result3['attributes'], "pan_step is not null: " + str(result3['attributes'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert None is result4['attributes'], "pan_step is not null: " + str(result4['attributes'])

    assert 409 == context.my_CCS_5.req.status_code, "ERROR. status_code: " + str(context.my_CCS_5.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_5.req.content)
    assert 'Fail validation attributes' == str(result5['message']), "Error in response: " + str(result5['message'])

    assert 409 == context.my_CCS_6.req.status_code, "ERROR. status_code: " + str(context.my_CCS_6.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_6.req.content)
    assert 'Fail validation attributes' == str(result6['message']), "Error in response: " + str(result6['message'])

    test_finished()


@given(u'set command for pan step attribute - 1001 - wrong - pan_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(1001, 'pan_step')

    test_finished()


@then(u'check if asserts are correct - 1001 - pan_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for pan step attribute - -1001 - wrong - pan_step')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(-1001, 'pan_step')

    test_finished()


@then(u'check if asserts are correct - -1001 - pan_step')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for go_to_home - null - OK - go_to_home')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_cogmmand(None, 'go_to_home')

    test_finished()


@given(u'set command for go_to_home - integer - wrong - go_to_home')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'go_to_home')

    test_finished()


@given(u'set command for go_to_home - string - wrong - go_to_home')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'go_to_home')

    test_finished()


@given(u'set command for go_to_home - bool - wrong - go_to_home')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(False, 'go_to_home')

    test_finished()


@then(u'check if asserts are correct - go_to_home')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@given(u'set command for go_to_poi - null - wrong - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'go_to_poi')

    test_finished()


@given(u'set command for go_to_poi - minimum_value - OK - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(1, 'go_to_poi')

    test_finished()


@given(u'wait time to move to the position selected - go_to_poi')
def step_impl(context):
    print()
    time.sleep(10)

    test_finished()


@given(u'set command for go_to_poi - intermediate_value - OK - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command(3, 'go_to_poi')

    test_finished()


@given(u'set command for go_to_poi - maximum_value - OK - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(5, 'go_to_poi')

    test_finished()


@given(u'set command for go_to_poi - string - wrong - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_5 = CameraControlService(mac, environment, username, password)
    context.my_CCS_5.set_command('string', 'go_to_poi')

    test_finished()


@given(u'set command for go_to_poi - bool - wrong - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_6 = CameraControlService(mac, environment, username, password)
    context.my_CCS_6.set_command(False, 'go_to_poi')

    test_finished()


@then(u'check if asserts are correct - go_to_poi')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()
    result5 = context.my_CCS_5.req.json()
    result6 = context.my_CCS_6.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    assert 200 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert None is result2['attributes'], "go_to_poi is not null: " + str(result2['attributes'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert None is result3['attributes'], "go_to_poi is not null: " + str(result3['attributes'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert None is result4['attributes'], "go_to_poi is not null: " + str(result4['attributes'])

    assert 409 == context.my_CCS_5.req.status_code, "ERROR. status_code: " + str(context.my_CCS_5.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_5.req.content)
    assert 'Fail validation attributes' == str(result5['message']), "Error in response: " + str(result5['message'])

    assert 409 == context.my_CCS_6.req.status_code, "ERROR. status_code: " + str(context.my_CCS_6.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_6.req.content)
    assert 'Fail validation attributes' == str(result6['message']), "Error in response: " + str(result6['message'])

    test_finished()


@given(u'set command for go_to_poi - 0 - wrong - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(0, 'go_to_poi')

    test_finished()


@then(u'check if asserts are correct - 0 - go_to_poi')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for go_to_poi - 6 - wrong - go_to_poi')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(6, 'go_to_poi')

    test_finished()


@then(u'check if asserts are correct - 6 - go_to_poi')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Fail validation attributes' == str(result1['message']), "Error in response: " + str(result1['message'])

    test_finished()


@given(u'set command for factory_reset - null - OK - factory_reset')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'factory_reset')

    test_finished()


@given(u'set command for factory_reset - integer - wrong - factory_reset')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'factory_reset')

    test_finished()


@given(u'set command for factory_reset - string - wrong - factory_reset')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'factory_reset')

    test_finished()


@given(u'set command for factory_reset - bool - wrong - factory_reset')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(False, 'factory_reset')

    test_finished()


@then(u'check if asserts are correct - factory_reset')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@given(u'set command for warm_reset - null - OK - warm_reset')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'warm_reset')

    test_finished()


@given(u'set command for warm_reset - integer - wrong - warm_reset')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'warm_reset')

    test_finished()


@given(u'set command for warm_reset - string - wrong - warm_reset')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'warm_reset')

    test_finished()


@given(u'set command for warm_reset - bool - wrong - warm_reset')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(False, 'warm_reset')

    test_finished()


@then(u'check if asserts are correct - warm_reset')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@given(u'set command for cold_reset - null - OK - cold_reset')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(None, 'cold_reset')

    test_finished()


@given(u'set command for cold_reset - integer - wrong - cold_reset')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'cold_reset')

    test_finished()


@given(u'set command for cold_reset - string - wrong - cold_reset')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'cold_reset')

    test_finished()


@given(u'set command for cold_reset - bool - wrong - cold_reset')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(False, 'cold_reset')

    test_finished()


@then(u'check if asserts are correct - cold_reset')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@given(u'set command for trigger_videoclip - bool - OK - trigger_videoclip')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_command(False, 'trigger_videoclip')

    test_finished()


@given(u'set command for trigger_videoclip - integer - wrong - trigger_videoclip')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(100, 'trigger_videoclip')

    test_finished()


@given(u'set command for trigger_videoclip - string - wrong - trigger_videoclip')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_command('string', 'trigger_videoclip')

    test_finished()


@given(u'set command for trigger_videoclip - null - wrong - trigger_videoclip')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(None, 'trigger_videoclip')

    test_finished()


@then(u'check if asserts are correct - trigger_videoclip')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail validation attributes' == str(result2['message']), "Error in response: " + str(result2['message'])

    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Fail validation attributes' == str(result3['message']), "Error in response: " + str(result3['message'])

    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Fail validation attributes' == str(result4['message']), "Error in response: " + str(result4['message'])

    test_finished()


@then(u'Perform a current_position while panning is on')
def step_impl(context):
    print()
    print("Get the status and current position of a camera...")
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.req = requests.post('https://' + context.my_CCS_2.environment +
                                         '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                         json={'attributes': ['current_position'], 'cameraMac': context.my_CCS_2.mac},
                                         auth=HTTPBasicAuth(context.my_CCS_2.username, context.my_CCS_2.password))

    # Checking by HTTP Response
    print("Status: " + str(context.my_CCS_2.req.status_code))
    print("Content: " + str(context.my_CCS_2.req.content))

    test_finished()


@then(u'check if asserts are correct - panning and current_position')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Motor is currently panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@then(u'Perform a home_position while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('home_position', None)

    test_finished()


@then(u'check if asserts are correct - panning and home_position')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Motor is currently panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@then(u'Perform a go_to_home while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(None, 'go_to_home')

    test_finished()


@then(u'check if asserts are correct - panning and go_to_home')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Tried to change pan while motor is panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@then(u'Perform a go_to_poi while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_command(1, 'go_to_poi')

    test_finished()


@then(u'check if asserts are correct - panning and go_to_poi')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Tried to change pan while motor is panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@then(u'Perform a point_of_interest while panning is on')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('point_of_interest', 2)

    test_finished()


@then(u'check if asserts are correct - panning and point_of_interest')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 'Fail - 7: Tried to change pan while motor is panning' == str(result2['status']),\
        "Error in response: " + str(result2['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    test_finished()


@when(u'delay for the panning - 1 minute')
def step_impl(context):
    print()
    print("Performing a delay of 1 minute.")
    time.sleep(60)

    test_finished()


@then(u'set command go_to_poi - position 1')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(1, 'go_to_poi')

    test_finished()


@then(u'check if position and asserts are correct - go_to_poi after panning')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])

    test_finished()


@then(u'set command go_to_home after panning')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_command(None, 'go_to_home')

    test_finished()


@then(u'check if position and asserts are correct - go_to_home after panning')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])

    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])

    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])

    test_finished()
