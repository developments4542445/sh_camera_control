# Perform tests over the getConfig_APIs.feature

from behave import *
from my_functions_class import *
import time
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'

my_CCS = CameraControlService(mac, environment, username, password)


@given(u'get the config attributes - horiz_flip')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('horiz_flip')

    test_finished()


@then(u'check if asserts are correct - horiz_flip')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if result['attributes'][0]['value'] is True:
        flag_response = True
    elif result['attributes'][0]['value'] is False:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'horiz_flip' == str(result['attributes'][0]['attribute']),\
        "Response was not horiz_flip: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - vert_flip')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('vert_flip')

    test_finished()


@then(u'check if asserts are correct - vert_flip')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if result['attributes'][0]['value'] is True:
        flag_response = True
    elif result['attributes'][0]['value'] is False:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'vert_flip' == str(result['attributes'][0]['attribute']),\
        "Response was not vert_flip: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - img_res')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('img_res')

    test_finished()


@then(u'check if asserts are correct - img_res')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if str(result['attributes'][0]['value']) == 'sd':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'hd':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'fhd':
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'img_res' == str(result['attributes'][0]['attribute']),\
        "Response was not img_res: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('motion_sensitivity')

    test_finished()


@then(u'check if asserts are correct - motion_sensitivity')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if str(result['attributes'][0]['value']) == 'low':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'mid':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'high':
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'motion_sensitivity' == str(result['attributes'][0]['attribute']),\
        "Response was not motion_sensitivity: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('videoclip_block_zone')

    test_finished()


@then(u'check if asserts are correct - videoclip_block_zone')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if str(result['attributes'][0]['attribute']) == 'videoclip_block_zone':
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'videoclip_block_zone' == str(result['attributes'][0]['attribute']),\
        "Response was not videoclip_block_zone: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['attribute'])

    test_finished()


@given(u'get the config attributes - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('sound_threshold')

    test_finished()


@then(u'check if asserts are correct - sound_threshold')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if str(result['attributes'][0]['value']) == 'low':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'mid':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'high':
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'sound_threshold' == str(result['attributes'][0]['attribute']),\
        "Response was not sound_threshold: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('day_night_threshold')

    test_finished()


@then(u'check if asserts are correct - day_night_threshold')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if str(result['attributes'][0]['value']) == 'low':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'mid':
        flag_response = True
    elif str(result['attributes'][0]['value']) == 'high':
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'day_night_threshold' == str(result['attributes'][0]['attribute']),\
        "Response was not day_night_threshold: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - day_night_automatic')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('day_night_automatic')

    test_finished()


@then(u'check if asserts are correct - day_night_automatic')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if result['attributes'][0]['value'] is True:
        flag_response = True
    elif result['attributes'][0]['value'] is False:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'day_night_automatic' == str(result['attributes'][0]['attribute']),\
        "Response was not day_night_automatic: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('point_of_interest')

    test_finished()


@then(u'check if asserts are correct - point_of_interest')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if 0 < result['attributes'][0]['value'] < 6:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'point_of_interest' == str(result['attributes'][0]['attribute']),\
        "Response was not point_of_interest: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])

    test_finished()


@given(u'get the config attributes - home_position')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('home_position')

    test_finished()


@then(u'check if asserts are correct - home_position')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if type(result['attributes'][0]['value']) is int:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'home_position' == str(result['attributes'][0]['attribute']),\
        "Response was not home_position: " + str(result['attributes'][0]['attribute'])

    test_finished()


@given(u'get the config attributes - alarm_state')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.get_config('alarm_state')

    test_finished()


@then(u'check if asserts are correct - alarm_state')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS_1.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    if result['attributes'][0]['value'] is True:
        flag_response = True
    elif result['attributes'][0]['value'] is False:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 'Success' == str(result['status']), "Response was not Success: " + str(result['status'])
    assert 'alarm_state' == str(result['attributes'][0]['attribute']),\
        "Response was not alarm_state: " + str(result['attributes'][0]['attribute'])
    assert flag_response is True, "Value in response is not correct: " + str(result['attributes'][0]['value'])
