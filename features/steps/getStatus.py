# Perform tests over the getStatus_APIs.feature

from behave import *
from my_functions_class import *
import time
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'

my_CCS = CameraControlService(mac, environment, username, password)


@given(u'get the status of the wifi level of a camera - wifi_level')
def step_impl(context):
    print()
    print("Get the status of wifi level of a camera...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['wifi_level'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - wifi_level')
def step_impl(context):
    print()
    # Checking by content
    result = context.req.json()

    # Asserts
    assert 200 == context.req.status_code, "status_code was not 200"
    assert 'wifi_level' == result['attributes'][0]['attribute'],\
        "Response was not 'wifi_level': " + result['attributes'][0]['attribute']
    assert result['attributes'][0]['value'] is not None, "Value in response is not correct: "\
                                                         + result['attributes'][0]['value']

    test_finished()


@given(u'get the status of the firmware version of a camera - fw_version')
def step_impl(context):
    print()
    print("Get the status of firmware version of a camera...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['fw_version'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - fw_version')
def step_impl(context):
    print()
    # Checking by content
    result = context.req.json()

    # Asserts
    assert 200 == context.req.status_code, "ERROR. status_code: " +\
                                           str(context.req.status_code) + '. content: ' + str(context.req.content)
    assert 'fw_version' == result['attributes'][0]['attribute'],\
        "Response was not 'fw_version': " + result['attributes'][0]['attribute']
    assert result['attributes'][0]['value'] is not None, "Value in response is not correct: "\
                                                         + result['attributes'][0]['value']

    test_finished()


@given(u'get the status of WiFi bandwidth of a camera - wifi_bw')
def step_impl(context):
    print()
    print("Get the status of wifi bandwidth of a camera...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['wifi_bw'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - wifi_bw')
def step_impl(context):
    print()
    # Checking by content
    result = context.req.json()

    # Asserts
    assert 200 == context.req.status_code, "ERROR. status_code: " +\
                                           str(context.req.status_code) + '. content: ' + str(context.req.content)
    assert 'wifi_bw' == result['attributes'][0]['attribute'],\
        "Response was not 'wifi_bw': " + result['attributes'][0]['attribute']
    assert result['attributes'][0]['value'] is not None, "Value in response is not correct: "\
                                                         + result['attributes'][0]['value']

    test_finished()


@given(u'get the status and current position of a camera - current_position')
def step_impl(context):
    print()
    print("Get the status and current position of a camera...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['current_position'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - current_position')
def step_impl(context):
    print()
    # Checking by content
    result = context.req.json()

    # Asserts
    assert 200 == context.req.status_code, "ERROR. status_code: " +\
                                           str(context.req.status_code) + '. content: ' + str(context.req.content)
    assert 'current_position' == result['attributes'][0]['attribute'],\
        "Response was not 'current_position': " + result['attributes'][0]['attribute']
    assert result['attributes'][0]['value'] is not None, "Value in response is not correct: "\
                                                         + result['attributes'][0]['value']

    test_finished()


@given(u'get the status of a non existent attribute')
def step_impl(context):
    print()
    print("Get the status of a non existence attribute...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['horiz_flip'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - non_existent_attribute')
def step_impl(context):
    print()

    # Asserts
    assert 409 == context.req.status_code, "ERROR. status_code: " +\
                                           str(context.req.status_code) + '. content: ' + str(context.req.content)

    test_finished()


@given(u'get the status of uSD-card presence of a camera - usd_presence')
def step_impl(context):
    print()
    print("Get the status of uSD-card presence of a camera...")
    context.req = requests.post('https://' + my_CCS.environment + '-vs.mirgor.com.ar/cameracontrol/getStatus',
                                json={'attributes': ['usd_presence'], 'cameraMac': my_CCS.mac},
                                auth=HTTPBasicAuth(my_CCS.username, my_CCS.password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if asserts are correct - usd_presence')
def step_impl(context):
    print()
    # Checking by content
    result = context.req.json()

    if result['attributes'][0]['value'] is True:
        flag_response = True
    elif result['attributes'][0]['value'] is False:
        flag_response = True
    else:
        flag_response = False

    # Asserts
    assert 200 == context.req.status_code, "ERROR. status_code: " +\
                                           str(context.req.status_code) + '. content: ' + str(context.req.content)
    assert 'usd_presence' == result['attributes'][0]['attribute'],\
        "Response was not 'usd_presence': " + result['attributes'][0]['attribute']
    assert flag_response is True, "Value in response is not correct: " + result['attributes'][0]['value']

    test_finished()
