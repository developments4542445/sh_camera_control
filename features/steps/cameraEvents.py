# Perform tests over the cameraEvents.feature

from behave import *
from my_functions_class import *
import time

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'


@given(u'Initializing the SmartCam')
def step_impl(context):
    print()
    input('Connect the camera. Press enter to continue.')
    print("Wait until camera indicates the advertisement: "
          "'Conexión establecida satisfactoriamente' and the led is green and not blinking."
          " After 2 minutes, if it is not listed this will automatically raise an error."
          " Press enter to continue.")

    test_finished()


@then(u'Checking that SmartCam is running')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)

    expired = False
    count = 0
    while expired is False:
        context.my_CCS_1.all_camera()
        count += 1
        if context.my_CCS_1.mac in str(context.my_CCS_1.req.json()):
            expired = True
            break
        if count >= 8:
            assert False, "Test finished due to SmartCam is not up and running. status_code: " + \
                   str(context.my_CCS_1.req.status_code) + '. content: ' + str(context.my_CCS_1.req.content)
        time.sleep(15)

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert context.my_CCS_1.mac in str(context.my_CCS_1.req.json()),\
        "Camera mac is not listed in the camera control service"

    test_finished()


@then(u'Perform an initSession')
def step_impl(context):
    print()
    context.my_CCS_1.perform_init_session()

    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)

    test_finished()


@then(u'Disconnect the SmartCam')
def step_impl(context):
    print()
    input('Disconnect the camera. Press enter to continue '
          '(20 seconds delay will be generated for video server updates).')
    time.sleep(20)  # Delay for the update of the camera in the camera control service of the video server

    test_finished()


@then(u'Checking that SmartCam is not listed')
def step_impl(context):
    print()
    context.my_CCS_1.all_camera()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert context.my_CCS_1.mac not in str(context.my_CCS_1.req.json()),\
        "Camera mac is not listed in the camera control service"

    test_finished()


@then(u'Checking that session is closed due to CCS')
def step_impl(context):
    print()
    context.my_CCS_1.perform_statistics_specs()

    print("Content: " + str(context.my_CCS_1.req.json()['content'][0]['eventClose']))

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert context.my_CCS_1.req.json()['content'][0]['maxSessionConcurrent'] == 1,\
        "maxSessionConcurrent is not 1: " + str(context.my_CCS_1.req.json()['content'][0]['maxSessionConcurrent'])
    assert str(context.my_CCS_1.req.json()['content'][0]['eventClose']) == 'CCS',\
        'State is not "CCS": ' + str(context.my_CCS_1.req.json()['content'][0]['eventClose'])

    test_finished()


@then(u'Disconnect and reconnect the SmartCam')
def step_impl(context):
    print()
    input('Disconnect the SmartCam, wait 5 seconds and reconnect it. Press enter to continue '
          '(30 seconds delay will be generated for video server updates).')
    time.sleep(30)  # Delay for the update of the camera in the camera control service of the video server

    test_finished()


@then(u'Wait 120 seconds after initialization')
def step_impl(context):
    print()
    print('A delay of 120 seconds will be generated. After this, SmartCam state will be checked again.')
    time.sleep(120)     # Delay for the update of the camera in the camera control service of the video server

    test_finished()


@then(u'Checking both initSession states')
def step_impl(context):
    print()
    context.my_CCS_1.perform_statistics_specs()

    print('First initSession state generated:')
    print("Content: " + str(context.my_CCS_1.req.json()['content'][1]))
    print('Second initSession state generated:')
    print("Content: " + str(context.my_CCS_1.req.json()['content'][0]))

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert str(context.my_CCS_1.req.json()['content'][1]['eventClose']) == 'CCS',\
        'State is not "CCS" in first initSession : ' + str(context.my_CCS_1.req.json()['content'][1]['eventClose'])
    assert context.my_CCS_1.req.json()['content'][1]['maxSessionConcurrent'] == 1,\
        "maxSessionConcurrent is not 1 in first initSession: " + \
        str(context.my_CCS_1.req.json()['content'][1]['maxSessionConcurrent'])

    assert str(context.my_CCS_1.req.json()['content'][0]['eventClose']) == 'CCS' or \
           context.my_CCS_1.req.json()['content'][0]['eventClose'] is None, \
           'State is not "CCS" in second initSession: ' + str(context.my_CCS_1.req.json()['content'][1]['eventClose'])
    assert context.my_CCS_1.req.json()['content'][0]['maxSessionConcurrent'] == 1,\
        "maxSessionConcurrent is not 1 in second initSession: " + \
        str(context.my_CCS_1.req.json()['content'][0]['maxSessionConcurrent'])

    test_finished()
