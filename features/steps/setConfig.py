# Perform tests over the setConfig_APIs.feature

from behave import *
from my_functions_class import *
import time
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'

my_CCS = CameraControlService(mac, environment, username, password)


@given(u'configure the camera - null - wrong - horiz_flip')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('horiz_flip', None)

    test_finished()


@given(u'configure the camera - int - wrong - horiz_flip')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('horiz_flip', 2)

    test_finished()


@given(u'configure the camera - string - wrong - horiz_flip')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('horiz_flip', 'string')

    test_finished()


@given(u'configure the camera - bool - OK - horiz_flip')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('horiz_flip', False)

    test_finished()


@then(u'check if configure asserts are correct - horiz_flip')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])
    assert 'horiz_flip' == str(result4['attributes'][0]['attribute']),\
        "Response was not horiz_flip: " + str(result4['attributes'][0]['attribute'])
    assert True is result4['attributes'][0]['value'], "Value in response is not correct: " +\
                                                      str(result4['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - null - wrong - vert_flip')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('vert_flip', None)

    test_finished()


@given(u'configure the camera - int - wrong - vert_flip')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('vert_flip', 2)

    test_finished()


@given(u'configure the camera - string - wrong - vert_flip')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('vert_flip', 'string')

    test_finished()


@given(u'configure the camera - bool - OK - vert_flip')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('vert_flip', False)

    test_finished()


@then(u'check if configure asserts are correct - vert_flip')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])
    assert 'vert_flip' == str(result4['attributes'][0]['attribute']),\
        "Response was not vert_flip: " + str(result4['attributes'][0]['attribute'])
    assert True is result4['attributes'][0]['value'], "Value in response is not correct: " +\
                                                      str(result4['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - null - wrong - img_res')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('img_res', None)

    test_finished()


@given(u'configure the camera - int - wrong - img_res')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('img_res', 2)

    test_finished()


@given(u'configure the camera - string - OK - img_res')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('img_res', 'hd')

    test_finished()


@given(u'configure the camera - bool - wrong - img_res')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('img_res', False)

    test_finished()


@then(u'check if configure asserts are correct - img_res')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])
    assert 'img_res' == str(result3['attributes'][0]['attribute']),\
        "Response was not img_res: " + str(result3['attributes'][0]['attribute'])
    assert 'true' == str(result3['attributes'][0]['value']), "Value in response is not correct: " + \
                                                             str(result3['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - wrong string - wrong - img_res')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.set_config('img_res', 'string')

    test_finished()


@then(u'check if configure asserts are correct - wrong string - img_res')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS.req.json()

    # Asserts
    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'configure the camera - null - wrong - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('motion_sensitivity', None)

    test_finished()


@given(u'configure the camera - int - wrong - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('motion_sensitivity', 2)

    test_finished()


@given(u'configure the camera - string - OK - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('motion_sensitivity', 'low')

    test_finished()


@given(u'configure the camera - bool - wrong - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('motion_sensitivity', False)

    test_finished()


@then(u'check if configure asserts are correct - motion_sensitivity')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])
    assert 'motion_sensitivity' == str(result3['attributes'][0]['attribute']),\
        "Response was not motion_sensitivity: " + str(result3['attributes'][0]['attribute'])
    assert 'true' == str(result3['attributes'][0]['value']), "Value in response is not correct: " + \
                                                             str(result3['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - wrong string - wrong - motion_sensitivity')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.set_config('motion_sensitivity', 'string')

    test_finished()


@then(u'check if configure asserts are correct - wrong string - motion_sensitivity')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS.req.json()

    # Asserts
    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'configure the camera - null - wrong - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('videoclip_block_zone', None)

    test_finished()


@given(u'configure the camera - int - wrong - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('videoclip_block_zone', 2)

    test_finished()


@given(u'configure the camera - string - wrong - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('videoclip_block_zone', 'string')

    test_finished()


@given(u'configure the camera - bool - wrong - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('videoclip_block_zone', False)

    test_finished()


@given(u'configure the camera - JSON - OK - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_5 = CameraControlService(mac, environment, username, password)
    context.my_CCS_5.set_config_json('videoclip_block_zone', 20, 0)

    test_finished()


@given(u'configure the camera - JSON - wrong x - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_6 = CameraControlService(mac, environment, username, password)
    context.my_CCS_6.set_config_json('videoclip_block_zone', 25, 0)

    test_finished()


@given(u'configure the camera - JSON - wrong y - videoclip_block_zone')
def step_impl(context):
    print()
    context.my_CCS_7 = CameraControlService(mac, environment, username, password)
    context.my_CCS_7.set_config_json('videoclip_block_zone', 0, 25)

    test_finished()


@then(u'check if configure asserts are correct - videoclip_block_zone')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()
    result5 = context.my_CCS_5.req.json()
    result6 = context.my_CCS_6.req.json()
    result6 = context.my_CCS_7.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)
    assert 200 == context.my_CCS_5.req.status_code, "ERROR. status_code: " + str(context.my_CCS_5.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_5.req.content)
    assert 409 == context.my_CCS_6.req.status_code, "ERROR. status_code: " + str(context.my_CCS_6.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_6.req.content)
    assert 409 == context.my_CCS_7.req.status_code, "ERROR. status_code: " + str(context.my_CCS_7.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_7.req.content)

    # Asserts
    assert 'Success' == str(result5['status']), "Response was not Success: " + str(result5['status'])

    test_finished()


@given(u'configure the camera - null - wrong - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('sound_threshold', None)

    test_finished()


@given(u'configure the camera - int - wrong - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('sound_threshold', 2)

    test_finished()


@given(u'configure the camera - string - OK - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('sound_threshold', 'low')

    test_finished()


@given(u'configure the camera - bool - wrong - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('sound_threshold', None)

    test_finished()


@then(u'check if configure asserts are correct - sound_threshold')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])
    assert 'sound_threshold' == str(result3['attributes'][0]['attribute']),\
        "Response was not sound_threshold: " + str(result3['attributes'][0]['attribute'])
    assert 'true' == str(result3['attributes'][0]['value']), "Value in response is not correct: " + \
                                                             str(result3['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - wrong string - wrong - sound_threshold')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.set_config('sound_threshold', 'string')

    test_finished()


@then(u'check if configure asserts are correct - wrong string - sound_threshold')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS.req.json()

    # Asserts
    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'configure the camera - null - wrong - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('day_night_threshold', None)

    test_finished()


@given(u'configure the camera - int - wrong - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('day_night_threshold', 2)

    test_finished()


@given(u'configure the camera - string - OK - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('day_night_threshold', 'low')

    test_finished()


@given(u'configure the camera - bool - wrong - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('day_night_threshold', False)

    test_finished()


@then(u'check if configure asserts are correct - day_night_threshold')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 200 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result3['status']), "Response was not Success: " + str(result3['status'])
    assert 'day_night_threshold' == str(result3['attributes'][0]['attribute']),\
        "Response was not day_night_threshold: " + str(result3['attributes'][0]['attribute'])
    assert 'true' == str(result3['attributes'][0]['value']), "Value in response is not correct: " + \
                                                             str(result3['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - wrong string - wrong - day_night_threshold')
def step_impl(context):
    print()
    context.my_CCS = CameraControlService(mac, environment, username, password)
    context.my_CCS.set_config('day_night_threshold', 'string')

    test_finished()


@then(u'check if configure asserts are correct - wrong string - day_night_threshold')
def step_impl(context):
    print()
    # Checking by content
    result = context.my_CCS.req.json()

    # Asserts
    assert 409 == context.my_CCS.req.status_code, "ERROR. status_code: " + str(context.my_CCS.req.status_code) + \
                                                  '. content: ' + str(context.my_CCS.req.content)

    test_finished()


@given(u'configure the camera - null - wrong - day_night_automatic')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('day_night_automatic', None)

    test_finished()


@given(u'configure the camera - int - wrong - day_night_automatic')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('day_night_automatic', 2)

    test_finished()


@given(u'configure the camera - string - wrong - day_night_automatic')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('day_night_automatic', 'string')

    test_finished()


@given(u'configure the camera - bool - OK - day_night_automatic')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('day_night_automatic', False)

    test_finished()


@then(u'check if configure asserts are correct - day_night_automatic')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])
    assert 'day_night_automatic' == str(result4['attributes'][0]['attribute']),\
        "Response was not day_night_automatic: " + str(result4['attributes'][0]['attribute'])
    assert True is result4['attributes'][0]['value'], "Value in response is not correct: " +\
                                                      str(result4['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - null - wrong - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('point_of_interest', None)

    test_finished()


@given(u'configure the camera - int - OK - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('point_of_interest', 2)

    test_finished()


@given(u'configure the camera - string - wrong - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('point_of_interest', 'string')

    test_finished()


@given(u'configure the camera - bool - wrong - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('point_of_interest', False)

    test_finished()


@then(u'check if configure asserts are correct - point_of_interest')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 200 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result2['status']), "Response was not Success: " + str(result2['status'])

    test_finished()


@given(u'configure the camera - value 0 - wrong - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('point_of_interest', 0)

    test_finished()


@given(u'configure the camera - value 6 - wrong - point_of_interest')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('point_of_interest', 6)

    test_finished()


@then(u'check if configure asserts are correct - wrong string - point_of_interest')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)

    test_finished()


@given(u'configure the camera - null - OK - home_position')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('home_position', None)

    test_finished()


@given(u'configure the camera - int - wrong - home_position')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('home_position', 4)

    test_finished()


@given(u'configure the camera - string - wrong - home_position')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('home_position', 'string')

    test_finished()


@given(u'configure the camera - bool - wrong - home_position')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('home_position', False)

    test_finished()


@then(u'check if configure asserts are correct - home_position')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 200 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 409 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result1['status']), "Response was not Success: " + str(result1['status'])
    assert 'home_position' == str(result1['attributes'][0]['attribute']),\
        "Response was not home_position: " + str(result1['attributes'][0]['attribute'])
    assert None is result1['attributes'][0]['value'], "Value in response is not correct: " +\
                                                      str(result1['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - null - wrong - alarm_state')
def step_impl(context):
    print()
    context.my_CCS_1 = CameraControlService(mac, environment, username, password)
    context.my_CCS_1.set_config('alarm_state', None)

    test_finished()


@given(u'configure the camera - int - wrong - alarm_state')
def step_impl(context):
    print()
    context.my_CCS_2 = CameraControlService(mac, environment, username, password)
    context.my_CCS_2.set_config('alarm_state', 4)

    test_finished()


@given(u'configure the camera - string - wrong - alarm_state')
def step_impl(context):
    print()
    context.my_CCS_3 = CameraControlService(mac, environment, username, password)
    context.my_CCS_3.set_config('alarm_state', 'string')

    test_finished()


@given(u'configure the camera - bool - OK - alarm_state')
def step_impl(context):
    print()
    context.my_CCS_4 = CameraControlService(mac, environment, username, password)
    context.my_CCS_4.set_config('alarm_state', False)

    test_finished()


@then(u'check if configure asserts are correct - alarm_state')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.my_CCS_1.req.json()
    result2 = context.my_CCS_2.req.json()
    result3 = context.my_CCS_3.req.json()
    result4 = context.my_CCS_4.req.json()

    # Asserts
    assert 409 == context.my_CCS_1.req.status_code, "ERROR. status_code: " + str(context.my_CCS_1.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_1.req.content)
    assert 409 == context.my_CCS_2.req.status_code, "ERROR. status_code: " + str(context.my_CCS_2.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_2.req.content)
    assert 409 == context.my_CCS_3.req.status_code, "ERROR. status_code: " + str(context.my_CCS_3.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_3.req.content)
    assert 200 == context.my_CCS_4.req.status_code, "ERROR. status_code: " + str(context.my_CCS_4.req.status_code) + \
                                                    '. content: ' + str(context.my_CCS_4.req.content)

    # Asserts
    assert 'Success' == str(result4['status']), "Response was not Success: " + str(result4['status'])
    assert 'alarm_state' == str(result4['attributes'][0]['attribute']),\
        "Response was not alarm_state: " + str(result4['attributes'][0]['attribute'])
    assert True is result4['attributes'][0]['value'], "Value in response is not correct: " +\
                                                      str(result4['attributes'][0]['value'])

    test_finished()


@given(u'configure the camera - first and second value - img_res')
def step_impl(context):
    print()
    context.req = requests.post('https://ist-vs.mirgor.com.ar/cameracontrol/setConfig',
                                json={'attributes': [{'attribute': 'img_res', 'value': 'sd'},
                                                     {'attribute': 'img_res', 'value': 'hd'}],
                                      'cameraMac': mac},
                                auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if configure asserts are correct - duplicated element')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.req.json()

    # Asserts
    assert 409 == context.req.status_code, "ERROR. status_code: " + str(context.req.status_code) + \
                                           '. content: ' + str(context.req.content)

    test_finished()


@given(u'configure the camera - equal values - img_res')
def step_impl(context):
    print()
    context.req = requests.post('https://ist-vs.mirgor.com.ar/cameracontrol/setConfig',
                                json={'attributes': [{'attribute': 'img_res', 'value': 'hd'},
                                                     {'attribute': 'img_res', 'value': 'hd'}],
                                      'cameraMac': mac},
                                auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(context.req.status_code))
    print("Content: " + str(context.req.content))

    test_finished()


@then(u'check if configure asserts are correct - equal values')
def step_impl(context):
    print()
    # Checking by content
    result1 = context.req.json()

    # Asserts
    assert 409 == context.req.status_code, "ERROR. status_code: " + str(context.req.status_code) + \
                                           '. content: ' + str(context.req.content)

    test_finished()
